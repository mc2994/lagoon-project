import { User } from '../models/user';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { PaginatedObject } from '../utils/paginated-object';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: User[];

  constructor(private http: HttpClient) { }

  getAllUsers(page: number, size: number): Observable<PaginatedObject<User>> {
    let params = new HttpParams();
    params = params.append('size', String(size));
    params = params.append('page', String(--page));

    const tokenUrl = 'http://localhost:8080/api/auth/user/getAllUsers';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.get<PaginatedObject<User>>(tokenUrl, { headers: headers, params: params });
  }

  getUserById(id: string) {}

  fetchUsersByName(fullName: string): Observable<PaginatedObject<User>> {
    const tokenUrl = 'http://localhost:8080/api/auth/user/fetchUsersByName/' + fullName;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.get<PaginatedObject<User>>(tokenUrl, { headers: headers });
  }

  fetchPhotosAync() {
    const tokenUrl = 'http://localhost:8080/api/auth/findPhotosAsync/';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.get(tokenUrl, { headers: headers });
  }

  getUserByName(userName: string) {
    const tokenUrl = 'http://localhost:8080/api/auth/user/userName/' + userName;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.get<User>(tokenUrl, { headers: headers });
  }

  updateUser(user: User): Observable<User> {
    const tokenUrl1 = 'http://localhost:8080/api/auth/user/update';
    const headers1 = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.post<User>(tokenUrl1, JSON.stringify(user), { headers: headers1 });
  }

  downloadPDF() {
    const url = 'http://localhost:8080/api/auth/pdf';
    const headers1 = new HttpHeaders({ 'Content-Type': 'application/octet-stream', 
                     'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.get(url, { headers: headers1, observe: 'response', responseType: 'blob' });
  }

  downloadExcel() {
    const url = 'http://localhost:8080/api/auth/excel';
    const headers1 = new HttpHeaders({ 'Content-Type': 'application/octet-stream', 
                    'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.get(url, { headers: headers1, observe: 'response', responseType: 'blob' });
  }

  downloadCSV() {
    const url = 'http://localhost:8080/api/auth/csv';
    const headers1 = new HttpHeaders({ 'Content-Type': 'application/octet-stream', 
                    'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.get(url, { headers: headers1, observe: 'response', responseType: 'blob' });
  }

}