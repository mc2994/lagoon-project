import { Observable } from 'rxjs/internal/Rx';
import { Injectable } from '@angular/core';
import { Photo } from '../models/photo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private http: HttpClient) { }

  getPhotos(): Observable<Photo[]> {
    const url = 'http://localhost:8080/api/auth/allPhotos';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.get<Photo[]>(url, { headers: headers });
  }

  getPhotoById(photoId: number): Observable<Photo> {
    const tokenUrl = 'http://localhost:8080/api/auth/photo/photoId';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.post<Photo>(tokenUrl, JSON.stringify(photoId), { headers: headers });
  }

  getPhotosByUser(user: User): Observable<Photo[]> {
    const tokenUrl = 'http://localhost:8080/api/auth/photo/user';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.post<Photo[]>(tokenUrl, JSON.stringify(user), { headers: headers });
  }

  updatePhoto(photo: Photo): Observable<Photo[]> {
    const tokenUrl = 'http://localhost:8080/api/auth/photo/update';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.post<Photo[]>(tokenUrl, JSON.stringify(photo), { headers: headers });
  }

}
