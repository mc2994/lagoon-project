import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {User} from '../models/user';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  constructor (private http: HttpClient) {}

  sendUser(user: User):Observable<User> {
    const url = 'http://localhost:8080/api/auth/register';
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post<User>(url, JSON.stringify(user), {headers: headers});
  }
}
